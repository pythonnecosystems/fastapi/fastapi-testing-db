# FastAPI: 데이터베이스 테스팅 <sup>[1](#footnote_1)</sup>

아직 들어보지 못하셨다면, FastAPI는 개발자가 최신 Python을 최대한 활용할 수 있게 해주는 마이크로 프레임워크이다. 여기서 '마이크로'는 모든 사용 사례를 다루기보다는 한 가지, 즉 빠른(물론) API를 구축할 수 있는 도구를 제공하는 데 중점을 둔다는 뜻이다. 그 외의 모든 것은 사용자에게 달려 있으므로 필요에 따라 아키텍처를 완벽하게 맞춤화할 수 있다.

하지만 다른 프레임워크에서는 일반적으로 기본으로 제공되는 몇 가지 사항을 직접 설정해야 한다는 것을 의미한다. 예를 들어 관계형 데이터베이스를 사용하려면 ORM을 선택해 설치해야 한다. SQLAlchemy는 Fast API에서 문서화된 것이다. 설정하는 데 도움이 되는 가이드와 테스트 방법에 대한 몇 가지 지침을 제공하는 튜토리얼이 있다. 하지만 장고(Django)를 사용했다면, 각 테스트가 독립적으로 작동하고 데이터베이스는 실행 전 상태로 유지되도록 모든 것을 구성하는 데 어려움을 겪을 수 있다.

이것이 바로 이 포스팅에서 다룰 내용이다!

이 포스팅에 소개된 코드는 [Github](https://github.com/jbrocher/hashnode-testing-database-fastapi)에서 완전히 사용할 수 있다. 리포지토리를 복제하고 `docker-compose up`을 하여 모든 것을 실행하기만 하면 된다.


<a name="footnote_1">1</a>: 이 페이지는 [FastAPI : Testing a Database](https://medium.com/@jeanb.rocher/setting-up-the-project-f24ccbec5f9f)를 편역한 것임.
