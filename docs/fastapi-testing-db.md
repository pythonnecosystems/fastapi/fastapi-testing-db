## 프로젝트 설정
먼저 테스트할 것이 필요하다! 제목과 설명이 있는 "항목"을 생성하고 데이터베이스에 저장하는 엔드포인트가 있는 작은 프로젝트를 만들어 보겠다. 우리는 `docker-compose`를 사용하여 Fastapi 이미지와 그 옆에 Postgres 데이터베이스를 실행하는 것을 선호한다. 하지만 필수는 아니다.

그럼 시작해 봅시다!

### SQLAlchemy 설정
SQLAlchemy는 [선언적 베이스(declarative base)](https://docs.sqlalchemy.org/en/14/orm/mapping_api.html#sqlalchemy.orm.declarative_base)를 생성하여 작동하며, 이를 통해 데이터베이스 테이블을 이를 상속하는 Python 클래스로 설명할 수 있다. `database.py`라는 파일에서 이를 구성해 보겠다.

```python
# database.py

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker # We're using postgres but you could use# any other engine supported by SQlAlchemy 


SQLALCHEMY_DATABASE_URL = "postgresql://test-fastapi:password@db/db" 
engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
```

이제 `Base`를 상속하여 Item 모델을 만들 수 있다.

### Models, schemas 및 CRUD utils
`Item` 모델의 경우, `models.py`라는 파일을 만들고 방금 구성한 `Base`를 사용하여 선언한다.

```python
# models.py

from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from .database import Base

class Item(Base): 
    __tablename__ = "items" # Don't forget to set an index ! 

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
```

또한 해당 [Pydantic](https://pydantic-docs.helpmanual.io/) 스키마도 정의해야 한다. FastAPI는 이를 사용하여 유효성 검사와 정규화를 수행한다.

```python
# schemas.py

from typing import Optional
from pydantic import BaseModel

classItemBase(BaseModel): 
    title: str
    description: Optional[str] = None
    
classItemCreate(ItemBase):
    pass
    
classItem(ItemBase): 
    id: int
    
    classConfig: 
        orm_mode = True
```

마지막으로 `Item` 인스턴스를 처리하기 위한 CRUD 유틸리티들도 있다.

```python
# crud.py

from sqlalchemy import select
from sqlalchemy.orm import Session
from . import schemas
from .models import Item


def get_items(db: Session): 
    items = select(Item)
    return db.execute(items).scalars().all()
    
def create_item(db: Session, item: schemas.ItemCreate): 
    db_item = Item(**item.dict())
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item
```

이제 데이터베이스와 관련된 모든 작업을 마쳤으니 테스트할 엔드포인트를 만들어 보겠다.

### 엔트포인트
`main.py` 파일을 만들고 다음 줄을 추가한다.

```python
# main.py

from typing import List
from fastapi import Depends
from fastapi import FastAPI
from sqlalchemy.orm import Session

from . import crud
from . import models
from . import schemas
from .database import engine
from .database import SessionLocal


app = FastAPI() 
# Here we create all the tables directly in the app# in a real life situation this would be handle by a migratin tool
# Like alembic 
models.Base.metadata.create_all(bind=engine)

# Dependency
def get_db(): 
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close() 

@app.get("/items/", response_model=List[schemas.Item])
def read_items(db: Session = Depends(get_db)):
    return crud.get_items(db)

@app.post("/items/", response_model=schemas.Item)
def create_item(item: schemas.ItemCreate, db: Session = Depends(get_db)):
    return crud.create_item(db, item)
```

엔드포인트에 대한 종속성으로 SQLAlchemy 세션을 전달한 것을 확인할 수 있다. 이는 다음 섹션에서 살펴볼 것처럼 엔드포인트를 쉽게 테스트하는 데 도움이 되므로 주의해야 한다.

Swagger 덕분에 `/docs` 엔드포인트에서 사용할 수 있는 엔드포인트가 작동하는지 확인할 수 있다. FastAPI의 또 다른 멋진 기능이다.

![](./images/0_fGuZTMyFqMBmyV5V.webp)

훌륭하다! 하지만 여기서 우리의 목표는 단위 테스트들을 통해 엔드포인트를 자동으로 테스트하는 것이다. 테스트가 메인 데이터베이스에 영향을 미치지 않도록 해야 한다. 또한 결정론적이고 재사용이 가능해야 하는데, 정확히 어떻게 하는지 알아보겠다.

## 테스팅 엔드포인트

### 기본 테스팅
먼저 공식 문서에 설명된 방법을 시도해 보겠다. 여기서 아이디어는 FastAPI 종속성 시스템을 활용하는 것이다. 엔드포인트는 종속성 주입을 통해 세션을 수신하므로 종속성 오버라이드를 사용하여 테스트 데이터베이스를 가리키는 세션으로 대체할 수 있다.

이를 위해 `test_database.py`라는 파일을 생성하여 테스트를 작성하고 다음 코드를 추가한다.

```python
# test_database.py

SQLALCHEMY_DATABASE_URL = "postgresql://test-fastapi:password@db/test-fastapi-test" engine = create_engine(SQLALCHEMY_DATABASE_URL)
  TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)defoverride_get_db():try:
          db = TestingSessionLocal()yield dbfinally:
          db.close()

  app.dependency_overrides[get_db] = override_get_db
```

이렇게 하면 `get_db` 종속성의 모든 인스턴스가 테스트 데이터베이스에 연결된 `Session`을 반환하는 `override_get_db`로 대체된다. 이렇게 하면 메인 데이터베이스에 영향을 주지 않고 테스트를 실행할 수 있다는 장점이 있다.

테스트를 작성하고 pytest로 실행하여 예상대로 작동하는지 확인해 보겠다.

```python
def test_post_items():  
    # We grab another session to check 
    # if the items are created db = override_get_db() 
    client = TestClient(app)

    client.post("/items/", json={"title": "Item 1"})

    client.post("/items/", json={"title": "Item 2"})

    items = crud.get_items(db)assert len(items) == 2
```

![](./images/0_Fc3-K7oGj-vGtwKA.webp)

완벽하게 작동한다! 우리는 또한 우리의 테스트가 결정론이 되기를 원하므로 pytest를 다시 실행하면 동일한 결과를 얻어야 한다...

![](./images/0_t7XbYIyV6FFd25Xa.webp)

하지만 그렇지 않다! 테스트를 처음 실행했을 때 생성된 항목이 아직 데이터베이스에 남아 있으므로 이제 2개가 아닌 4개가 있다. 이제 수동으로 항목을 삭제할 수 있다. 그러나 더 복잡한 동작을 검증하는 테스트가 많아지면 유지 관리가 매우 지루해지기 때문에 이 방법은 아니다.

다행히도 더 좋은 방법이 있다!

### 트랜잭션으로


### 어디서나 fixtures!


## 여기까지

